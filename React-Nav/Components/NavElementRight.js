import React from 'react';

class NavElementRight extends React.Component{
  render(){
    return(
      <div className="nav-element-right">
          {this.props.children}
      </div>
    )
  }
}

export {NavElementRight};