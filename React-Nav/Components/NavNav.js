import React from 'react';
import './../NavCSS/NavBar.css';

class NavNav extends React.Component{
  render(){
    return(
      <div className="navbar">
        <ul className="navbar-menu">
          {this.props.children}
        </ul>
      </div>
    )
  }
}

export {NavNav};