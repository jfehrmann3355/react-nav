import React from 'react';

function NavLogo(props){
  return(
    <div className="nav-logo">
      <img className="image-logo" src={props.image} alt=""/>
    </div>
  )
}

export {NavLogo};