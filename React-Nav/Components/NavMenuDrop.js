import React from 'react';
import {Link} from 'react-router-dom';

class NavMenuDrop extends React.Component{
  constructor (props){
    super(props);
    this.state={
      showNavElement:false
    }
    this.namedrop=props.name;
  }

  showNavElement(){
    this.setState({
      showNavElement:true
    });
  }
  hideNavElement(){
    this.setState({
      showNavElement:false
    });
  }

  render(){
    return(
      <li onMouseEnter={()=>this.showNavElement()} onMouseLeave={()=>this.hideNavElement()}><Link>{this.namedrop}</Link>
      {
      this.state.showNavElement?
        <ul id="navElement">
          {this.props.children}
        </ul>
      :null
      }
      </li>
    )
  }
}

export {NavMenuDrop};