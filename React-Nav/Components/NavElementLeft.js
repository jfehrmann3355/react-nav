import React from 'react';

class NavElementLeft extends React.Component{
  render(){
    return(
      <div className="nav-element-left">
          {this.props.children}
      </div>
    )
  }
}

export {NavElementLeft};