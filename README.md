# React-Nav

# INSTRUCCIONES DE USO:

* Para usar React se requiere la instalación del modulo '_react-router-dom_'. Utilice el comando npm install '_react-router-dom_'
para incorporarlo a tu proyecto react.

* Al descargar la carpeta React-Nav se puede incorporar la librería en cualquier ruta de su proyecto.

* Para implementar la librería de componentes en su proyecto debe incorporarla de la siguiente manera:
import {"Componentes"} from './mi ruta/React-Nav'

* Los componentes se dividen entre obligatorios (*) y no obligatorios.

- NavNav*           - Parametros: 
- NavElementLeft*   - Parametros:
- NavElementRight*  - Parametros:
- NavMenuList       - Parametros: name="Nombre", link="Dirección"
- NavItem           - Parametros: name="Nombre", link="Dirección"
- NavMenuDrop       - Parametros: name="Nombre"
- NavLogo           - Parametros: image="Ruta imagen"

* Ejemplos de la arquitectura:

  import Logo from './imageF.svg.png';

  import { NavNav, NavLogo, NavMenuList, NavMenuDrop, NavItem, NavElementLeft, NavElementRight } from './React-Nav';
  
  import {BrowserRouter as Router} from 'react-router-dom';

      <NavNav>
        <NavLogo image={Logo}/>
        <NavElementRight>
          <NavMenuList name='List 1' link="Example 1"/>
          <NavMenuList name='List 2' link="Example 2"/>

          <NavMenuDrop name='DropMenu'>
            <NavItem name='DropItem 3' link="Example 3"/>
            <NavItem name='DropItem 4' link="Example 4"/>
          </NavMenuDrop>
          <NavMenuDrop name='DropMenu'>
            <NavItem name='DropItem 5' link="Example 5"/>
            <NavItem name='DropItem 6' link="Example 6"/>
          </NavMenuDrop>
        </NavElementRight>
      </NavNav>

* Configuración de los estilos css:
En la carpeta React-Nav se encuentra una carpeta llamada NavCSS y un archivo NavBar.css. Para configurar los estilos ingresamos al archivo .css y configuramos todo el archivo si así lo queremos o en su defecto configuramos la sección de settings si es suficiente.

Ejemplo de configuración de barra de navegación oscura:

.navbar{
  /*Primary background color*/
  --primary-navbar:#FFF;

  /*Secondary background color*/
  --secondary-navbar: #FFF;

  /*Element and item color*/
  --element-color: #000;
  --item-color: #000;
  --item-background:#FFF;

  /*Navbar shadow box*/
  --shadow-box-navbar: 0px 0px 10px 0px #111;

  /*Height navbar*/
  --height-navbar: 70px;

  /*Navbar position left/right*/
  --left-element: 100px;
  --right-element: 60px;

  /*Space between elemens and items*/
  --width-element: 120px;
  --width-item: 120px;

  /*Hover color*/
  --element-background-hover: #AAA;
  --item-background-hover: #AAA;
  --element-color-hover: #FFF;
  --item-color-hover: #EEE;

  /*Logo settings*/
  --logo-width: 180px;
  --logo-height: 70px;
  --logo-position: 20px;
}


